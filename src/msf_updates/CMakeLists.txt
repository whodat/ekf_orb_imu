add_definitions(-DBOOST_NO_CXX11_VARIADIC_TEMPLATES)
add_library(pose_distorter msf_distort/PoseDistorter.cc)

add_executable(pose_position_msf pose_position_msf/pose_msf_test.cpp ${PCL_INCLUDE_DIRS})
target_link_libraries(pose_position_msf msf_core msf_timing pose_distorter ${PCL_LIBRARIES})

#add_executable(pose_position_msf_lins_test ${YAML_CPP_INCLUDE_DIR} pose_position_msf/pose_msf_lins_test.cpp)
#target_link_libraries(pose_position_msf_lins_test ${YAML_CPP_LIBRARIES} msf_core msf_timing pose_distorter ${PCL_LIBRARIES})
